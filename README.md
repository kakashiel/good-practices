# Good Practices

This project is an intended to summarize all good & interesting practices to optimize the development of applications.

## Git

### Git commit message
Following the conventional commits message: https://www.conventionalcommits.org/en/v1.0.0/
Exemple of type: 

build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)

chore: Other changes that don't modify source or test files

ci: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)

docs: Documentation only changes

feat: A new feature

fix: A bug fix

perf: A code change that improves performance

refactor: A code change that neither fixes a bug nor adds a feature

revert: Reverts a previous commit

style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)

test: Adding missing tests or correcting existing tests
